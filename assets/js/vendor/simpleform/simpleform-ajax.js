jQuery( document ).ready( function($){
    var simpleform = $('#simpleform'),
    simpleformUrl = 'https://getsimpleform.com/messages/ajax?form_api_token=b6483d2bb49cacefae828f38f72e2f2c';

    $('#turn-on-js').hide();
    $('#simpleform :input').each(function(){
        $(this).show();
    })

    simpleform.submit( function( event ) {
        event.preventDefault();

        var simplesubmit = $('#simplesubmit'),
            name = $('#name'),
            email = $('#email'),
            message = $('#message');

        simplesubmit.prop( 'disabled', true );
        $('#simpleform [class*="message"]').stop(true,true).hide();

        if( name.val() !== "" && email.val() !== "") {
            $('#simpleform .message--sending').show();

            var formData = {
                'name'    : name.val(),
                'email'   : email.val(),
                'message' : message.val()
            };

            $.ajax({
                type: 'POST',
                url: simpleformUrl,
                data: formData,
                dataType: 'jsonp'
            }).always(function(){
                $('#simpleform .message--sending').hide();
                simplesubmit.prop( 'disabled', false );
            }).done(function( data ) {
                $('#simpleform .message--success').show().delay(5000).fadeOut();
                $('#simpleform').trigger("reset");
            }).fail(function( data ) {
                $('#simpleform .message--fail').html('Message sending failed. You can email me directly at: <a href="mailto:hey@fuzzyrobotdiscobear.com">hey@fuzzyrobotdiscobear.com</a>').show();
            });
        }
    })
});