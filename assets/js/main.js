document.addEventListener( 'DOMContentLoaded', function() {
    var mainNavLinks = document.querySelectorAll('header nav .page-link'),
        homeNav = document.getElementsByClassName('home-nav')[0],
        homeArrow = document.getElementById('more-content-indicator'),
        mobileMenuToggle = document.getElementById('menu-toggle'),
        siteTitle = document.querySelector('#site-title h1');

    function scrollToHome( event ) {
        event.preventDefault();
        window.scrollTo({
            behavior: 'smooth',
            left: 0,
            top:  0
        })
    }

    (function createBackToTop() {
        var backToTopButton = document.createElement('button'),
            siteFooter = document.getElementsByClassName('site-footer')[0];

        backToTopButton.className = 'back-to-top',
        backToTopButton.title = 'Back to top',
        backToTopButton.innerHTML = 'Back to top';

        document.body.insertBefore( backToTopButton, siteFooter );

        backToTopButton.addEventListener( 'click', function( event ) {
            scrollToHome(event);
        })

        siteTitle.style.cursor = 'pointer';
        siteTitle.addEventListener( 'click', function( event ) {
            scrollToHome(event);
        })

        var waypoint = new Waypoint({
            element: document.querySelector('main section:nth-child(2)'),
            handler: function( direction ) {
                ( direction == 'down' ? backToTopButton.style.opacity = '1' : backToTopButton.style.opacity = '0' );
            },
            offset: '100vh'
        })
    })();

    mainNavLinks.forEach( function( element ) {
        var targetId = document.getElementById( element.dataset.scrollto );
        element.addEventListener( 'click', function( event ) {
            event.preventDefault();
            mobileMenuToggle.checked = false;
            window.scrollTo({
                behavior: 'smooth',
                left: 0,
                top:  targetId.offsetTop
            })
        })

        var waypoint = new Waypoint({
            element: targetId,
            handler: function() {
              // console.log(element.dataset.scrollto + ' came into view ');
            },
            offset: '50%'
        })
    })

    homeArrow.addEventListener( 'click', function() {
        window.scrollTo({
            behavior: 'smooth',
            left: 0,
            top: window.innerHeight
        })
    })

    /*
     * MOUSETRAP STUFF
     */
    Mousetrap.bind('up up down down left right left right b a enter', function() {
        console.log('Konami code');
    });
})
